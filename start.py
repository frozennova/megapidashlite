import serial, time, re, mserial, sys, signal,os
from datetime import datetime
from oled.serial import i2c
from oled.device import ssd1306, sh1106
from oled.render import canvas
from PIL import ImageFont, ImageDraw, Image
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

#Setup OLED

# rev.1 users set port=0
seriali2c = i2c(port=1, address=0x3C)

# substitute sh1106(...) below if using that device
device = sh1106(seriali2c)

#setup error handler

def signal_handler(signal, frame):
	print("\nUser aborted program")
	GPIO.cleanup()
	sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

#Setup Serial Connection
ser = serial.Serial()
ser.port = "/dev/ttyUSB0"
ser.baudrate = 115200
ser.bytesize = serial.EIGHTBITS #number of bits per bytes
ser.parity = serial.PARITY_NONE #set parity check: no parity
ser.stopbits = serial.STOPBITS_ONE #number of stop bits
ser.timeout = 1            #non-block read
ser.xonxoff = False     #disable software flow control
ser.rtscts = False     #disable hardware (RTS/CTS) flow control
ser.dsrdtr = False       #disable hardware (DSR/DTR) flow control
ser.writeTimeout = 2     #timeout for write
csvchar = ','
#Setup variables
injectorFlow = 650
injectorFlow = float(injectorFlow)

DeadTime = 0.9
DeadTimeCorrection = 0.1
connected = 0
currentPage = 0
degrees = "\xb0"


ecuData = {}
abspath = os.path.abspath(__file__)
scriptdir = os.path.dirname(abspath)
logoImage = scriptdir + "/mx5.png"
fontFile = scriptdir + "/DroidSans.ttf"

#Create Display headers
oledHeader = ["Time"]
oledHeader += ["Fuel Flow"]
oledHeader += ["Coolant"]
oledHeader += ["Inlet Temp"]
oledHeader += ["AFR"]
oledHeader += ["EGO Correction"]
oledHeader += ["Duty Cycle"]
oledHeader += ["Boost/Vac"]
oledHeader += ["Battery Voltage"]

lastPage = len(oledHeader)

#Start button press handler
GPIO.setup(18, GPIO.IN, pull_up_down=GPIO.PUD_UP)
def buttonInput(channel): 
	global currentPage 
	global lastPage 
	global connected
	currentPage += 1 
	if (currentPage == lastPage):
    		currentPage = 0 
	if (connected == 0):
		os.system('sudo shutdown now -h')


GPIO.add_event_detect(18, GPIO.FALLING, callback=buttonInput,bouncetime=200)


# Open debug file for appending
debugfile = open(scriptdir + "/logs/" + "megapidash.log", 'a')
debugfile.write(datetime.now().strftime('%d-%m-%Y %H.%M.%S' + ": Starting...\n"))
debugfile.flush()

while (True):
	
	with canvas(device) as draw:

		logo = Image.open(logoImage)
		draw.bitmap((2, 20), logo, fill=1)
		font = ImageFont.truetype(fontFile,14)
		size = font.getsize("No Connection")
		draw.text(( 64 - (size[0] / 2), 45), str("No Connection"), font=font, fill="white", align="center")
		
	debugfile.write(datetime.now().strftime('%d-%m-%Y %H.%M.%S' + ": Attempting to connect...\n"))
	debugfile.flush()
	
	#Loop through connection attempt
	
	while (connected == 0 ):
		try: 
			ser.open()
		except Exception, e:

			debugfile.write(datetime.now().strftime('%d-%m-%Y %H.%M.%S' + ": " + str(e) + " Sleeping for 15 seconds\n"))
			debugfile.flush()
			time.sleep(2)
			break
		
		# Flush Serial Buffers
		ser.flushInput() 
		ser.flushOutput()
		
		# Request Signature
		ser.write("Q")
		time.sleep(0.5)
		response= ser.readline()

		if(response == ""):


			with canvas(device) as draw:

				logo = Image.open(logoImage)
				draw.bitmap((2, 20), logo, fill=1)
				font = ImageFont.truetype(fontFile,14)
				size = font.getsize("No Connection")
				draw.text(( 64 - (size[0] / 2), 45), str("No Connection"), font=font, fill="white", align="center")		
				
			debugfile.write(datetime.now().strftime('%d-%m-%Y %H.%M.%S' + ": Could not connect...Waiting 15 seconds\n"))
			debugfile.flush()
			time.sleep(2)
			ser.close()
		else:
			connected = 1
			debugfile.write(datetime.now().strftime('%d-%m-%Y %H.%M.%S' + ": Connected to " + response + "\n"))
			debugfile.flush()

	if (connected == 1):
					
		#Reset trip data		
		ecuData['Time'] = 0
		
		
	while ( connected == 1 ):
		try: 
			#Loop read commands
			startTime = time.time()
			
			#Flush Serial I/O buffers
			ser.flushInput() 
			ser.flushOutput()

			#write data
			ser.write("A")

			#Read first 200 bytes and convert to decimal
			response = ser.read(200)
			
			# Error trap response
			if(response == ""):
				debugfile.write(datetime.now().strftime('%d-%m-%Y %H.%M.%S' + ": Connection lost...No Data Recieved\n"))
				debugfile.flush()
				connected = 0
				ser.close()
				break
						
			data = response.encode('hex')
			data = re.findall('..',data)
			
			
			FrameTime = (time.time() - startTime)
			outputTime = time.time()
			
			#Format returned data

			ecuData['Time'] += FrameTime
			ecuData['MAT'] = mserial.formatEcuData(data,20,21,-320,0.05555)
			ecuData['CLT'] = mserial.formatEcuData(data,22,23,-320,0.05555)
			ecuData['EGOCor'] = mserial.formatEcuData(data,34,35,0,0.1)
			ecuData['RPM'] = mserial.formatEcuData(data,6,7)
			ecuData['PW'] = mserial.formatEcuData(data,2,3,0,0.000666)
			ecuData['DutyCycle'] = mserial.calculateDutyCycle(ecuData['RPM'],ecuData['PW'])
			ecuData['AFR'] = mserial.formatEcuData(data,28,29,0,0.1)
			ecuData['TPS'] = mserial.formatEcuData(data,24,25,0,0.1)
			ecuData['MAP'] = mserial.formatEcuData(data,18,19,0,0.1)
			ecuData['Baro'] = mserial.formatEcuData(data,16,17,0,0.1)
			ecuData['Voltage'] = mserial.formatEcuData(data,26,27,0,0.1)
			ecuData['engine'] = int(data[11],16)
			ecuData['gpioport1'] = int(data[167],16)
			ecuData['gpioport2'] = int(data[168],16)
			calcDeadTime = ((ecuData['Voltage'] - 13.2) * DeadTimeCorrection) + DeadTime
			ecuData['LPH'] = mserial.calculateLph(ecuData['RPM'],injectorFlow,ecuData['PW'],calcDeadTime)
			
			ecuData['boostVac'] = ecuData['MAP'] - ecuData['Baro']
			if ( ecuData['MAP'] > ecuData['Baro'] ):
				ecuData['boostVac'] = ecuData['boostVac'] * 0.1450
				boostUnits = "psi"
			else:
				ecuData['boostVac'] = ecuData['boostVac'] * 0.2953007
				boostUnits = "in/hg"

			oledData = [datetime.now().strftime('%H:%M')]
			oledData += [str(round(ecuData['LPH'],2)) + "l/ph"]
			oledData += [str(round(ecuData["CLT"],1)) + degrees + "c"]
			oledData += [str(round(ecuData["MAT"],1)) + degrees + "c"]
			oledData += [str(round(ecuData["AFR"],1)) + ":1"]
			oledData += [str(round(ecuData["EGOCor"],1)) + "%"]
			oledData += [str(round(ecuData["DutyCycle"],2)) + "%"]
			oledData += [str(round(ecuData["boostVac"],2)) + boostUnits]
			oledData += [str(round(ecuData["Voltage"],2)) + "v"]
		
			with canvas(device) as draw:
				font = ImageFont.truetype(fontFile,14)

				size = font.getsize(str(oledHeader[currentPage]))
				draw.text(( 64 - (size[0] / 2), 5), str(oledHeader[currentPage]), font=font, fill="white", align="center")

				font = ImageFont.truetype(fontFile,22)
				size = font.getsize(oledData[currentPage])
				draw.text((64-(size[0] / 2), 20), oledData[currentPage], font=font, fill="white", align="center")

				font = ImageFont.truetype(fontFile,10)
				draw.rectangle((6, 48, 118, 58), fill="white")

				
				if (ecuData['engine'] & 8):
					font = ImageFont.truetype(fontFile,10)
					draw.text((8, 48), "WUE", font=font, fill="black", align="center")

				if (ecuData['gpioport2'] & 4):
					draw.text((32, 48), "PUMP", font=font, fill="black", align="center")
				else:
					draw.text((32, 48), "METH", font=font, fill="black", align="center")
					
				if (ecuData['gpioport2'] & 1 != 1):
					draw.text((63, 48), "VALET", font=font, fill="black", align="center")
				
				if (ecuData['gpioport1'] & 32):
					draw.text((97, 48), "FAN", font=font, fill="black", align="center")
					
			outputTime = time.time() - outputTime
			ecuData['Time'] += outputTime
			
		# Catch any exceptions in above block and throw generic error
		except Exception,e:
			print e
			debugfile.write(datetime.now().strftime('%d-%m-%Y %H.%M.%S' + ": Unknown Error...Waiting 15 seconds\n"))
			debugfile.flush()
			time.sleep(2)
			ser.close()
			connected = 0
			break
debugfile.close()
GPIO.cleanup()
